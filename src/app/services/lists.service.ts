import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, Observable } from 'rxjs';
import { CredentialsService } from './credentials.service';
import { Lists } from '../lists';
import { Board } from '../board';
import { LoaderService } from './loader.service';

@Injectable({
  providedIn: 'root',
})
export class ListsService {
  constructor(
    private http: HttpClient,
    private credentials: CredentialsService,
    public loader: LoaderService
  ) {}

  getLists(boardId: string): Observable<Lists[]> {
    this.loader.show();
    return this.http
      .get<Lists[]>(
        'https://api.trello.com/1/boards/' +
          boardId +
          '/lists?fields=id,name,closed,idBoard&key=' +
          this.credentials.getKey() +
          '&token=' +
          this.credentials.getToken()
      )
      .pipe(
        catchError((err) => {
          console.log(err);
          this.loader.hide();
          return [];
        })
      );
  }

  getBoard(boardId: String): Observable<Board> {
    return this.http.get<Board>(
      'https://api.trello.com/1/boards/' +
        boardId +
        '?key=' +
        this.credentials.getKey() +
        '&token=' +
        this.credentials.getToken()
    );
  }

  createList(name: string, boardId: string) {
    return this.http.post<Lists>(
      'https://api.trello.com/1/lists' +
        '?key=' +
        this.credentials.getKey() +
        '&token=' +
        this.credentials.getToken(),
      {
        name: name,
        idBoard: boardId,
      }
    );
  }

  deleteList(id: string) {
    return this.http.put<Lists>(
      'https://api.trello.com/1/lists/' +
        id +
        '/closed?key=' +
        this.credentials.getKey() +
        '&token=' +
        this.credentials.getToken(),
      {
        value: true,
      }
    );
  }
}
