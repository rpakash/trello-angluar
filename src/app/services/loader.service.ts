import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LoaderService {
  visibility: boolean = false;
  constructor() {}
  show() {
    this.visibility = true;
  }
  hide() {
    this.visibility = false;
  }
}
