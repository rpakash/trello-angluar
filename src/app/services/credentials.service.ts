import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CredentialsService {
  key: string = 'c079c9d915eb31613473b7e39ec8bbe3';
  token: string =
    'e0d68e65e0cfe22ff80571d05501c408baa6c37ec989164206120e977278e0fb';
  constructor() {}

  getKey() {
    return this.key;
  }

  getToken() {
    return this.token;
  }
}
