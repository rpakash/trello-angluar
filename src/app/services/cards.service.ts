import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cards } from '../cards';
import { CredentialsService } from './credentials.service';

@Injectable({
  providedIn: 'root',
})
export class CardsService {
  constructor(
    private http: HttpClient,
    private credentials: CredentialsService
  ) {}

  getCardsOfBoard(idBoard: string): Observable<Cards[]> {
    return this.http.get<Cards[]>(
      'https://api.trello.com/1/boards/' +
        idBoard +
        '/cards?key=' +
        this.credentials.key +
        '&token=' +
        this.credentials.token
    );
  }

  createNewCard(name: string, idList: string) {
    return this.http.post<Cards>(
      'https://api.trello.com/1/cards?key=' +
        this.credentials.key +
        '&token=' +
        this.credentials.token,
      {
        name: name,
        idList: idList,
      }
    );
  }

  deleteCard(id: string) {
    // return this.http.delete
    return this.http.delete(
      'https://api.trello.com/1/cards/' +
        id +
        '?key=' +
        this.credentials.key +
        '&token=' +
        this.credentials.token
    );
  }
}
