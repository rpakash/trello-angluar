import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, Observable } from 'rxjs';
import { Boards } from '../boards';
import { CredentialsService } from './credentials.service';
import { LoaderService } from './loader.service';

@Injectable({
  providedIn: 'root',
})
export class BoardsService {
  constructor(
    private http: HttpClient,
    private credentials: CredentialsService,
    public loader: LoaderService
  ) {}

  getBoards(): Observable<Boards[]> {
    this.loader.show();
    return this.http
      .get<Boards[]>(
        'https://api.trello.com/1/members/me/boards?fields=id,name,prefs,shortLink&key=' +
          this.credentials.getKey() +
          '&token=' +
          this.credentials.getToken()
      )
      .pipe(
        catchError((err) => {
          console.log(err);
          this.loader.hide();
          return [];
        })
      );
  }

  createBoard(name: string): Observable<Boards> {
    return this.http.post<Boards>(
      'https://api.trello.com/1/boards?fields=id,name,prefs,shortLink&key=' +
        this.credentials.getKey() +
        '&token=' +
        this.credentials.getToken(),
      {
        name: name,
      }
    );
  }
}
