export interface Lists {
  id: string;
  name: string;
  closed: boolean;
  idBoard: string;
}
