export interface Cards {
  id: string;
  name: string;
  shortLink: string;
  idList: string;
  idBoard: string;
}
