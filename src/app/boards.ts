export interface Boards {
  id: string;
  name: string;
  prefs: {
    backgroundImageScaled: {
      url: string;
    }[];
    backgroundColor: String;
  };
  shortLink: string;
}
