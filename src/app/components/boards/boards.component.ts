import { Component, Inject } from '@angular/core';
import { Boards } from 'src/app/boards';
import { BoardsService } from 'src/app/services/boards.service';
import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogRef,
} from '@angular/material/dialog';
import { catchError } from 'rxjs';
import { LoaderService } from 'src/app/services/loader.service';

export interface DialogData {
  name: string;
  createBoard: Function;
}

@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.css'],
})
export class BoardsComponent {
  name!: String;
  public boards!: Boards[];
  constructor(
    public board: BoardsService,
    private dialog: MatDialog,
    public loader: LoaderService
  ) {}
  dialogRef!: MatDialogRef<AddBoard>;
  showLoader: boolean = false;
  ngOnInit() {
    this.showLoader = true;
    this.board.getBoards().subscribe((items) => {
      this.boards = items;
      this.loader.hide();
    });
  }

  openDialog() {
    this.dialogRef = this.dialog.open(AddBoard, {
      width: '300px',
      data: { name: this.name, createBoard: this.createBoard },
    });
  }

  createBoard = (boardName: string) => {
    this.board.createBoard(boardName).subscribe((board) => {
      this.boards.push(board);
      this.dialogRef.close();
    });
  };
}

@Component({
  selector: 'add-board',
  templateUrl: './add-board.html',
  styleUrls: ['./boards.component.css'],
})
export class AddBoard {
  constructor(
    public dialogRef: MatDialogRef<AddBoard>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  onCreate() {
    this.data.createBoard(this.data.name);
  }
}
