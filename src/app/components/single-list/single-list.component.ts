import { Component, Input } from '@angular/core';
import { Cards } from 'src/app/cards';
import { Lists } from 'src/app/lists';
import { CardsService } from 'src/app/services/cards.service';
@Component({
  selector: 'app-single-list',
  templateUrl: './single-list.component.html',
  styleUrls: ['./single-list.component.css'],
})
export class SingleListComponent {
  @Input() list!: Lists;
  @Input() cards!: Cards[];
  @Input() openDialog!: Function;

  showAddCard: Boolean = false;
  newCardName: string = '';

  constructor(public cardService: CardsService) {}

  toggleShowAddCard() {
    this.showAddCard = !this.showAddCard;
  }

  ngOnInit() {
    console.log(this.list);
  }

  createNewCard() {
    if (this.newCardName.trim() === '') {
      return;
    }
    this.cardService
      .createNewCard(this.newCardName, this.list.id)
      .subscribe((card) => {
        this.cards.push(card);
        this.newCardName = '';
      });
  }
}
