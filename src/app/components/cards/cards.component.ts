import { Component, Input } from '@angular/core';
import { Cards } from 'src/app/cards';
import { CardsService } from 'src/app/services/cards.service';
CardsService;
@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css'],
})
export class CardsComponent {
  @Input() cards!: Cards[];

  constructor(private cardsService: CardsService) {}

  deleteCard(idCard: string) {
    // this.cardsService;

    this.cardsService.deleteCard(idCard).subscribe(() => {
      this.cards = this.cards.filter((card) => card.id !== idCard);
    });
  }
}
