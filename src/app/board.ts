export interface Board {
  id: string;
  name: string;
  prefs: {
    backgroundImage: string;
    backgroundColor: String;
  };
}
