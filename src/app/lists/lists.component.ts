import { Component, Inject, Pipe } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ListsService } from '../services/lists.service';
import { Lists } from '../lists';
import { Board } from '../board';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { LoaderService } from '../services/loader.service';
import { Cards } from '../cards';
import { CardsService } from '../services/cards.service';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css'],
})
export class ListsComponent {
  lists!: Lists[];
  newListName: string = '';
  showAddList: Boolean = false;
  board!: Board;
  dialogRef!: MatDialogRef<DeleteList>;
  cards!: Cards[];

  constructor(
    private route: ActivatedRoute,
    private listsService: ListsService,
    public dialog: MatDialog,
    public loader: LoaderService,
    private cardsServie: CardsService
  ) {}

  openDialog = (name: string, id: string): void => {
    console.log(name, id);
    console.log(this);
    this.dialogRef = this.dialog.open(DeleteList, {
      width: '300px',
      data: { name: name, id: id, delete: this.deleteList },
    });
  };

  deleteList = (id: string) => {
    this.listsService.deleteList(id).subscribe(() => {
      this.lists = this.lists.filter((list) => list.id !== id);
      this.dialogRef.close();
    });
  };

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('boardId') || '';

    this.listsService.getLists(id).subscribe((lists) => {
      this.lists = lists;

      this.cardsServie.getCardsOfBoard(id).subscribe((cards) => {
        this.cards = cards;
        this.loader.hide();
      });
    });

    this.listsService.getBoard(id).subscribe((board) => {
      this.board = board;
    });
  }

  toggleShowAddList() {
    this.showAddList = !this.showAddList;
  }

  createNewList = () => {
    this.listsService
      .createList(this.newListName, this.board.id)
      .subscribe((list) => {
        this.lists.push(list);
        this.toggleShowAddList();
        this.newListName = '';
      });
  };
}

@Component({
  selector: 'delete-list',
  templateUrl: 'Delete-list.html',
})
export class DeleteList {
  constructor(
    public dialogRef: MatDialogRef<DeleteList>,
    @Inject(MAT_DIALOG_DATA)
    public data: { name: string; id: string; delete: Function }
  ) {}
}

@Pipe({
  name: 'filterBy',
})
export class FilterPipe {
  transform(value: Cards[], id: string): Cards[] {
    let fieldValue = id;
    return value.filter((e) => {
      return e['idList'] === fieldValue;
    });
  }
}
